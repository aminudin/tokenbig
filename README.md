# TokenBesar Bot

Donation paypal to amin4udin@gmail.com

**Step**

autoRegist.js **->** emailGrabber.js or multipleMailGrab.js **->** verification.js



## Installation


```terminal
$ git clone this project
$ cd folderproject/
$ npm install
```

## Usage

```node
$ node script.js
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)