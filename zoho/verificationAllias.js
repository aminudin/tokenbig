const fetch = require("node-fetch");
const UsernameGenerator = require("username-generator");
const HttpsProxyAgent = require("https-proxy-agent");
const delay = require("delay");
const moment = require("moment");
const colors = require("../lib/colors");
const { URLSearchParams } = require("url");

const functionRegister = username =>
  new Promise((resolve, reject) => {
    const params = new URLSearchParams();
    params.append("smId", smId);
    params.append("emailId", "amin.4udin@gmail.com");
    params.append("mode", "frmVerify");
    params.append("vCode", codeVeryf);
    params.append(
      "zmrcsr",
      `f3f8bf4b0995af470cf18bae5bcd784f95ff1aa19ea9a45d4e02fd7bccdca0fa17db0c6181bdd0a0262663095ebe8a9bc9910a604bd95e93cbf70de2d1bcefba`
    );

    fetch("https://mail.zoho.com/zm/sendMailAs.do", {
      method: "POST",
      body: params,
      headers: {
        "cache-Control": "no-cache",
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        cookie: `zohocares-_zldp=YfEOFpfOAG8nTIMkZEgjIjrlm%2FHNjyBU%2F3K%2B477MDRNFtTe%2B51axpKgLzlb2AaL1pRUK595EVd8%3D; stk=900cbcd04c488fb5d6820ff0a161aea7; dcl_pfx_lcnt=0; zohocares-_zldt=12b6164e-703c-4edb-896a-c22aad56cb7d; _iamadt=1f890d52b731b87c34314ec75e4dc8ee3f28acc83c88d839d421426ef8804ee0d7d9533d8cd4cc8e12aa0a959b389b9491949968f39fd6c88bface138593e085; _iambdt=1cccab41c35c258918e79ae304d90fe54aeca988af28c10260cb847f70236f9594bf8c0ce2a01ea37fc2041b71977ea398950768be66a6ddef305bff92e078a2; _z_identity=_z_identity; dcl_pfx=us; dcl_bd=zoho.com; is_pfx=false; 9c8984d0f9=8c6ef617941ffe79dfd31e8331d08c71; zmcsr=f3f8bf4b0995af470cf18bae5bcd784f95ff1aa19ea9a45d4e02fd7bccdca0fa17db0c6181bdd0a0262663095ebe8a9bc9910a604bd95e93cbf70de2d1bcefba; zmuac=Njg4NTQ2MzY4; zmirc=-1; 2c2cf6c3b4=ef4d412f48af8280f41bdbec4faa404c; _iampt=688546367.688546368.c871a9b31fde2c6535efc7f0ada33b502e3ca1137bd6cc7648045a410e6695e094cc1db1d9237557d22029dee43f25908b30a0e4cd3be5754647835dd1c4a18b; 3a707640b4=7c9316ce580880c35519e829fe0919c9; 01b52bd07f=1d67f6f06b6a474bd9539c7e66f299c4; aprmjrnpkcrkks=f3f8bf4b0995af470cf18bae5bcd784f95ff1aa19ea9a45d4e02fd7bccdca0fa17db0c6181bdd0a0262663095ebe8a9bc9910a604bd95e93cbf70de2d1bcefba; 5779efe0b5=e411c920cc41a00bcf62b6879cea5a2c; concsr=f3f8bf4b0995af470cf18bae5bcd784f95ff1aa19ea9a45d4e02fd7bccdca0fa17db0c6181bdd0a0262663095ebe8a9bc9910a604bd95e93cbf70de2d1bcefba; b315c52c8b=4b5f8dd690d7d975312d1efa7f82d4cf; 9c2a003733=5b2d4dac69c13792809c972afa540132; CT_CSRF_TOKEN=f3f8bf4b0995af470cf18bae5bcd784f95ff1aa19ea9a45d4e02fd7bccdca0fa17db0c6181bdd0a0262663095ebe8a9bc9910a604bd95e93cbf70de2d1bcefba; com_chat_owner=1556532941948; zcalirc=-1; JSESSIONID=66FC9CFB21459C2C199E9D41BC52FEF8`,
        referer: "https://mail.zoho.com/zm/",
        "user-agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"
      }
    })
      .then(res => res.json())
      .then(text => resolve(text))
      .catch(err => reject(err));
  });

const genSes = length =>
  new Promise((resolve, reject) => {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    resolve(text);
  });

(async function main() {
  try {
    await console.log("");
    await console.log("");
    // for (let index = 0; index < 1000; index++) {
    const id = await genSes(32);

    const username = UsernameGenerator.generateUsername();
    const regist = await functionRegister(username);
    console.log(regist);
    // }
  } catch (e) {
    console.log(e);
  }
})();
